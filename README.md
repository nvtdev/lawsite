# [Start Bootstrap](http://startbootstrap.com/) - [Agency](http://startbootstrap.com/template-overviews/agency/)
> This fork is an unofficial MeteorJS ready clone of the Agency one-page theme

[Agency](http://startbootstrap.com/template-overviews/agency/) is a one page agency portfolio theme for [Bootstrap](http://getbootstrap.com/) created by [Start Bootstrap](http://startbootstrap.com/). This theme features several content sections, a responsive portfolio grid with hover effects, full page portfolio item modals, and a responsive timeline.

## Getting Started
> Note for the latest release without Meteor support, go to [the official Start Bootstrap site](http://startbootstrap.com/template-overviews/agency/)

1. Open terminal and download the repository:

  ```
  git clone https://github.com/KyleKing/meteor-startbootstrap-agency
  cd meteor-startbootstrap-agency
  ```

2. If you don't have meteor installed, install it with:

  ```
  curl https://install.meteor.com | /bin/sh
  ```

3. Run Meteor [[Guide ](http://docs.meteor.com)]:

  ```
  meteor
  ```

4. Point your browser to [Localhost:3000 ](http://localhost:3000)
5. Then you should see this:

  ![screenshot](public/github/screenshot.png)

## To Be Added:

- (Working) PHP contact form

## Bugs and Issues

Have a bug or an issue with this theme? [Open a new issue](https://github.com/KyleKing/startbootstrap-agency/issues)

## Creator

Start Bootstrap was created by and is maintained by **David Miller**, Managing Partner at [Iron Summit Media Strategies](http://www.ironsummitmedia.com/).

* https://twitter.com/davidmillerskt
* https://github.com/davidtmiller

Start Bootstrap is based on the [Bootstrap](http://getbootstrap.com/) framework created by [Mark Otto](https://twitter.com/mdo) and [Jacob Thorton](https://twitter.com/fat).

## Copyright and License

Copyright 2013-2015 Iron Summit Media Strategies, LLC. Code released under the [Apache 2.0](https://github.com/IronSummitMedia/startbootstrap-agency/blob/gh-pages/LICENSE) license.
