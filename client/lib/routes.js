FlowRouter.route('/', {
   name: 'home' ,
   action() {
       BlazeLayout.render('MainLayout', {navbar: 'navbar_main',
                                         content: 'content_main'});
   }
});
